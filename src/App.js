import React, { useState, useEffect } from 'react';
import './App.css';
import {
  TodoFormContextHook,
  ListContextHook
} from './components';
import { useStyles } from './constants';
import { useHttp } from './hooks';
import { TodoProvider } from './contexts';

import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';

function AppContextHook() {
  // set a new todo with api
  const [state, setState] = useState({
    newTodo: {},

    todos: [
      {
        text: 'Learn about React',
        isCompleted: false,
        selected: false,
      }
    ],

    removeIndex: -1,

    rCModal: false
  });

  // remove confirmation modal
  const [http] = useHttp(state.newTodo, [state.newTodo]);

  const classes = {
    modalStyle: useStyles.modal(),
    buttonStyle: useStyles.button(),
  };

  /**
   * @description replacement of componentDidMount, componentDidUpdate, componentWillUnmount
   * @param { () => {} } function executes when useEffect is triggered
   * @param { [] } data which will trigger useEffect when it is changed
   */
  useEffect(() => {
    if (!http.loading && http.response) {
      const newTodos = [...state.todos, http.response];
      setState({ ...state, todos: newTodos });
    }

    // componentWillUnmount
    return () => {
      // console.log('unmounting previous state');
    };
  }, [http]);

  // componentDidMount
  useEffect(() => {

    // componentWillUnmount
    return () => {
      // console.log('unmounting app component');
    };
  }, []);

  const addTodo = text => {
    setState({ ...state, newTodo: { text } });
  };

  const removeTodo = removeIndex => {
    const { todos } = state;
    todos[removeIndex].selected = true;
    // start removing process
    setState({ ...state, removeIndex, rCModal: true });
  };

  const triggerRemoveTodo = () => {
    const { todos, removeIndex } = state;
    todos.splice(removeIndex, 1);
    // close modal
    setState({ ...state, removeIndex: -1, rCModal: false });
  };

  const handleClose = () => {
    const { todos, removeIndex } = state;
    if (todos[removeIndex] && todos[removeIndex].selected) {
      todos[removeIndex].selected = false;
    }
    // close modal
    setState({ ...state, removeIndex: -1, rCModal: false });
  };

  return (
    <TodoProvider
      value={{
        todos: state.todos,
        removeIndex: state.removeIndex,
        addTodo,
        removeTodo
      }}
    >
      <div className="App">
        <TodoFormContextHook />
        <ListContextHook />
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modalStyle.modal}
          open={state.rCModal}
          onClose={handleClose}
          closeAfterTransition
          disableEnforceFocus
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 400,
          }}
        >
          <Fade in={state.rCModal}>
            <div className={classes.modalStyle.paper}>
              <h3 id="transition-modal-title">Are you sure?</h3>
              <p id="transition-modal-description">The selected item will be removed.</p>
              <div className="flex-container space-around">
                <Button variant="outlined" size="medium" color="primary" className={classes.buttonStyle.margin} onClick={handleClose}>
                  Cancel
                </Button>
                <Button variant="contained" size="medium" color="secondary" className={classes.buttonStyle.margin} onClick={triggerRemoveTodo}>
                  Remove
                </Button>
              </div>
            </div>
          </Fade>
        </Modal>
      </div>
    </TodoProvider>
  );
}

export default AppContextHook;
