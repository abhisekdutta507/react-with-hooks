import { createContext } from 'react';

const ContextTodo = createContext({
  todos: [],
  // rCModal: false,
  removeIndex: -1,

  addTodo: text => {},
  removeTodo: index => {},
  // triggerRemoveTodo: () => {},
  // handleClose: () => {},
});

export const TodoProvider = ContextTodo.Provider;
export const TodoConsumer = ContextTodo.Consumer;

export default ContextTodo;
