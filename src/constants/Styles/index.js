import { makeStyles } from '@material-ui/core/styles';
import ModalStyle from './Modal';
import ButtonStyle from './Button';

const modal = makeStyles((theme) => ({
  ...ModalStyle(theme),
}));

const button = makeStyles((theme) => ({
  ...ButtonStyle(theme),
}));

const useStyles = {
  modal,
  button
};

export default useStyles;