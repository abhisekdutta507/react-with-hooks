import { useState, useEffect } from 'react';

export const intialState = {
  loading: false,
  response: undefined,
  error: undefined
};

export const useHttp = (data, dependencies) => {
  const [state, setState] = useState(intialState);

  useEffect(() => {
    setState({ ...state, loading: true });
    {/*
      fetch(`https://nodeapis101.herokuapp.com/api/v1/${url}`).then(r => {
        return r.json();
      }).then(r => {
        setState({ ...state, response: r, loading: false });
      }).catch(e => {
        setState({ ...state, error: e, loading: false });
      });
    */}
    if (!data.text) setState({ ...intialState });
    else setState({
      ...state,
      loading: false,
      response: {
        text: data.text,
        isCompleted: false,
        selected: false,
      }
    });
  }, dependencies);

  return [state];
}
