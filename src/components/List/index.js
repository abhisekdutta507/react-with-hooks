// memo is used to replace shouldComponentUpdate
import React, { useContext } from 'react';
// import './todo.css';

import { ContextTodo } from '../../contexts';
import { TodoContextHook } from '../';

const ListContextHook = () => {
  const { todos, removeTodo } = useContext(ContextTodo);

  return (
    <div className="todo-list">
      {
        todos.map((todo, index) => (
          <TodoContextHook key={index} index={index} todo={todo} removeTodo={removeTodo} />
        ))
      }
    </div>
  );
};

export {
  ListContextHook
};
