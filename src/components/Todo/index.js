// memo is used to replace shouldComponentUpdate
import React, { useRef, useContext } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import './todo.css';

import { ContextTodo } from '../../contexts';

const TodoContextHook = ({todo, index, removeTodo}) => {
  const className = `
    todo-item
    todo-item-${index}
    ${index % 2 === 0 ? 'grey' : ''}
    ${todo.selected ? 'selected--remove' : ''}
  `;

  return (
    <div className={className}>
      <div className={'todo-text'}>{todo.text}</div>
      <FontAwesomeIcon className={`fa ${todo.selected ? 'white' : 'danger'}`} icon={faTimesCircle} onClick={e => removeTodo(index)} />
    </div>
  );
};

const TodoFormContextHook = () => {
  const context = useContext(ContextTodo);
  const formElement = useRef();

  const handleSubmit = e => {
    e.preventDefault();

    const form = formElement.current;
    const { todo } = form;

    if(!todo?.value) return;
    context.addTodo(todo?.value);
   
    // reset the form
    form.reset();
  }

  return (
    <form ref={formElement} onSubmit={handleSubmit}>
      <input type="text" name="todo" className="todo-input" placeholder="add todo"></input>
    </form>
  );
};

export {
  TodoContextHook,
  TodoFormContextHook
};
